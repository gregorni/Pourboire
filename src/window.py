# window.py
#
# Copyright 2023 Pourboire Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import math
import re

from gi.repository import Adw, Gio, Gtk


@Gtk.Template(resource_path="/io/gitlab/gregorni/Pourboire/gtk/window.ui")
class PourboireWindow(Adw.ApplicationWindow):
    __gtype_name__ = "PourboireWindow"

    tip_per_person_row = Gtk.Template.Child()
    total_per_person_row = Gtk.Template.Child()

    bill_entry_row = Gtk.Template.Child()
    tip_percent_spin_row = Gtk.Template.Child()
    people_num_spin_row = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.settings = Gio.Settings(schema_id="io.gitlab.gregorni.Pourboire")
        bind_flags = Gio.SettingsBindFlags.DEFAULT
        self.settings.bind("window-width", self, "default-width", bind_flags)
        self.settings.bind("window-height", self, "default-height", bind_flags)
        self.settings.bind("window-is-maximized", self, "maximized", bind_flags)

        self.bill_entry_row.connect("changed", self.evaluate_inputs)
        self.tip_percent_spin_row.connect("changed", self.evaluate_inputs)
        self.people_num_spin_row.connect("changed", self.evaluate_inputs)

        self.settings.bind(
            "tip-percent", self.tip_percent_spin_row, "value", bind_flags
        )
        self.settings.bind(
            "people-number", self.people_num_spin_row, "value", bind_flags
        )

        self.bill_entry_row.set_text(self.settings.get_string("bill"))

    def evaluate_inputs(self, *args):
        entered_bill = self.bill_entry_row.get_text()

        excusable_values = ("", "0")
        if entered_bill in excusable_values:
            self.bill_entry_row.remove_css_class("error")
            return

        only_contains_digits = re.search(r"[^0-9.]", entered_bill)
        if only_contains_digits:
            self.bill_entry_row.add_css_class("error")
            return

        bill = float(entered_bill)

        self.bill_entry_row.remove_css_class("error")
        tip_in_percent = self.tip_percent_spin_row.get_value()
        number_of_people = self.people_num_spin_row.get_value()

        bill_per_person = bill / number_of_people

        strict_tip_per_person = bill_per_person * tip_in_percent / 100
        total_per_person = bill_per_person + strict_tip_per_person

        round_total = bool(self.settings.get_value("round-total"))
        if round_total:
            total_per_person = math.ceil(total_per_person / 5) * 5

        tip_per_person = total_per_person - bill_per_person

        def __as_nice_string(float_num):
            float_num = round(float_num, 2)
            int_num = int(float_num)

            return_value = int_num

            if int_num != float_num:
                return_value = float_num

            return str(return_value)

        self.tip_per_person_row.set_subtitle(__as_nice_string(tip_per_person))
        self.total_per_person_row.set_subtitle(__as_nice_string(total_per_person))
